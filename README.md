# About

This is a library for event based simulation of application state, like the state of a video game. It's especially meant to represent the state of progress, like choices you made or quests you finished. While it might be possible to represent everything, it's not intended to represent inventory counts or the player position using this system.

# Features

- Separation of info and state: The library separates the simulation into immutable info and mutable state, allowing for flexible and reusable simulation setups.
- Support for different simulation engines: The library can handle various types of simulation engines, ranging from simple linear progress to complex Petri nets or state machines.
- Flexibility in representing application state: The library provides a general framework for representing and managing the state of an application, making it adaptable to different domains and use cases.

# Structure

The simulation is separated into two main structures, the info and the state. The info is meant to be immutable and might be shared by multiple simulations, while the state will be mutated by events.

The state cannot be created directly since it might rely on information from the info. So, the info also acts as a factory for states. An info can create default states, load states from simulation-specific data, or clone an existing state.

The simulation maintains both the info and the state, ensuring that the state cannot run with an invalid simulation. You can still obtain immutable references to the state, but you will typically interact with the simulation directly.

# Interaction

The info defines the structure of the application, containing information about what's possible under which conditions. The state defines the conditions under which specific events are possible.

The simulation will list a number of events. The application will provide interactable objects for each event, which the user can choose from. These objects could be a simple list or events in a game world that appear at specific places. By choosing an event, the state of the simulation will change. The simulation will then list which new events are available and which events have been disabled, so the application can update the interactable objects accordingly.

# Limitations

As mentioned before this library is not intended for representing all kinds of states. It focuses on representing the progress and choices made within an application.
