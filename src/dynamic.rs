use std::ops::Deref;

use crate::{Editable, SimulationInfo};

/// A dynamic type, able to store simulation infos or editable simulations.
pub enum DynamicSimulation<S: SimulationInfo, E: Editable> {
    /// Variant for the default `S` type.
    Default(S),
    /// Variant for the `E` type.
    Simulated(E),
}

impl<S: SimulationInfo, E: Editable> DynamicSimulation<S, E> {
    /// Convert the dynamic simulation into a `S`
    pub fn default(&self) -> Option<&S> {
        if let Self::Default(info) = self {
            Some(info)
        } else {
            None
        }
    }
    /// Convert the dynamic simulation into a mutable `S`
    pub fn default_mut(&mut self) -> Option<&mut S> {
        if let Self::Default(info) = self {
            Some(info)
        } else {
            None
        }
    }
    /// Convert the dynamic simulation into a `E`
    pub fn simulated(&self) -> Option<&E> {
        if let Self::Simulated(sim) = self {
            Some(sim)
        } else {
            None
        }
    }
    /// Convert the dynamic simulation into a mutable `E`
    pub fn simulated_mut(&mut self) -> Option<&mut E> {
        if let Self::Simulated(sim) = self {
            Some(sim)
        } else {
            None
        }
    }
}

impl<S: SimulationInfo, E: Editable + Deref<Target = S>> Deref for DynamicSimulation<S, E> {
    type Target = S;
    fn deref(&self) -> &S {
        match self {
            Self::Default(info) => info,
            Self::Simulated(sim) => sim,
        }
    }
}

impl<S: SimulationInfo, E: Editable> From<S> for DynamicSimulation<S, E> {
    fn from(info: S) -> Self {
        Self::Default(info)
    }
}
