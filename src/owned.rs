use std::ops::{Deref, DerefMut};

use crate::{Editable, EditableSimulationInfo};

use super::{Simulation, SimulationInfo};

/// An owned simulation that holds the simulation info and the simulation state.
pub struct OwnedSimulation<Info: SimulationInfo> {
    info: Info,
    /// The simulation state.
    pub state: Info::State,
}

impl<Info: SimulationInfo> OwnedSimulation<Info> {
    /// Creates a new `OwnedSimulation` using the specified `info`.
    pub fn new<T: Into<Info>>(info: T) -> Self {
        let info = info.into();
        let state = info.default_state();
        Self { info, state }
    }

    /// Loads a new `OwnedSimulation` from `data` using the specified `info`.
    pub fn from_data(info: Info, data: Info::LoadData) -> Result<Self, Info::StateLoadingError> {
        let state = info.load_state(data)?;
        Ok(Self { info, state })
    }

    /// Release the info from the simulation again and destroys all states.
    pub fn release(self) -> Info {
        self.info
    }
}

impl<Info: SimulationInfo + Clone> Clone for OwnedSimulation<Info> {
    fn clone(&self) -> Self {
        let info = self.info.clone();
        let state = unsafe { info.clone_state(&self.state) };
        Self { info, state }
    }
}

impl<Info: SimulationInfo> Deref for OwnedSimulation<Info> {
    type Target = Info;

    fn deref(&self) -> &Info {
        &self.info
    }
}

impl<Info: SimulationInfo> Simulation for OwnedSimulation<Info> {
    type StateLoadingError = Info::StateLoadingError;
    type AccessData = Info::AccessData;
    type LoadData = Info::LoadData;
    type Event = Info::Event;
    type EventContainer<'a>
        = Info::EventContainer<'a>
    where
        Self: 'a;

    #[inline]
    fn data(&self) -> &Info::AccessData {
        unsafe { self.info.data(&self.state) }
    }

    #[inline]
    fn reload(&mut self, data: Info::LoadData) -> Result<(), Info::StateLoadingError> {
        self.state = self.info.load_state(data)?;
        Ok(())
    }

    #[inline]
    fn callables(&self) -> Info::EventContainer<'_> {
        Info::callables(&self.state)
    }

    #[inline]
    fn callable(&self, event: Info::Event) -> bool {
        Info::callable(&self.state, event)
    }

    #[inline]
    unsafe fn call(&mut self, event: Info::Event) {
        unsafe { self.info.call(&mut self.state, event) }
    }

    #[inline]
    fn revertables(&self) -> Info::EventContainer<'_> {
        Info::revertables(&self.state)
    }

    #[inline]
    fn revertable(&self, event: Info::Event) -> bool {
        Info::revertable(&self.state, event)
    }

    #[inline]
    unsafe fn revert(&mut self, event: Info::Event) {
        unsafe { self.info.revert(&mut self.state, event) }
    }
}

impl<Info: EditableSimulationInfo> Editable for OwnedSimulation<Info> {
    type Edit<'a>
        = OwnedSimulationEdit<'a, Info>
    where
        Self: 'a;

    fn edit(&mut self) -> OwnedSimulationEdit<'_, Info> {
        let edit = unsafe { self.info.edit() };
        OwnedSimulationEdit {
            edit,
            state: &mut self.state,
        }
    }
}

/// Helper type for safely editing the info of a owned simulation without invalidating the state.
pub struct OwnedSimulationEdit<'a, Info: EditableSimulationInfo + 'a> {
    edit: Info::Edit<'a>,
    state: &'a mut Info::State,
}

impl<Info: EditableSimulationInfo> Drop for OwnedSimulationEdit<'_, Info> {
    fn drop(&mut self) {
        unsafe { self.edit.refresh_state(self.state) }
    }
}

impl<'a, Info: EditableSimulationInfo> Deref for OwnedSimulationEdit<'a, Info> {
    type Target = Info::Edit<'a>;
    fn deref(&self) -> &Info::Edit<'a> {
        &self.edit
    }
}

impl<'a, Info: EditableSimulationInfo> DerefMut for OwnedSimulationEdit<'a, Info> {
    fn deref_mut(&mut self) -> &mut Info::Edit<'a> {
        &mut self.edit
    }
}
